//
//  SpreadsheetViewController.m
//  Simple Spreadsheet
//
//  Created by Dawid on 03/08/2014.
//  Copyright (c) 2014 Dawid. All rights reserved.
//

#import <MMSpreadsheetView/MMSpreadsheetView.h>
#import <MMSpreadsheetView/NSIndexPath+MMSpreadsheetView.h>
#import "SpreadsheetViewController.h"
#import "SpreadsheetModelController.h"
#import "MMGridCell.h"
#import "MMTopRowCell.h"
#import "MMLeftColumnCell.h"

@interface SpreadsheetViewController () <MMSpreadsheetViewDataSource, MMSpreadsheetViewDelegate>
@property (nonatomic, strong) SpreadsheetModelController *modelController;
@property (nonatomic, strong) NSMutableSet *selectedGridCells;
@property (nonatomic, strong) NSMutableArray *tableData;
@property (nonatomic, strong) NSString *cellDataBuffer;
@property (nonatomic, strong) MMSpreadsheetView *spreadSheetView;
@property (nonatomic, strong) SSModelData *selectedDataModel;
@end


@implementation SpreadsheetViewController

- (instancetype)initWithModelController:(SpreadsheetModelController*)modelController
{
    self = [super init];
    if(self){
        self.modelController = modelController;
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSUInteger rows = 1+[self.modelController cols];
    NSUInteger cols = 1+[self.modelController rows];
    
    // Create some fake grid data for the demo.
    self.tableData = [NSMutableArray array];
    
    for (NSUInteger rowNumber = 0; rowNumber < rows; rowNumber++) {
        NSMutableArray *row = [NSMutableArray array];
        for (NSUInteger columnNumber = 0; columnNumber < cols; columnNumber++) {
            [row addObject:[NSString stringWithFormat:@"R%lu:C%lu", (unsigned long)rowNumber, (unsigned long)columnNumber]];
        }
        [self.tableData addObject:row];
    }
    
    self.selectedGridCells = [NSMutableSet set];
    
    // Create the spreadsheet in code.
    self.spreadSheetView = [[MMSpreadsheetView alloc] initWithNumberOfHeaderRows:1 numberOfHeaderColumns:1 frame:self.view.bounds];
    
    // Register your cell classes.
    [self.spreadSheetView registerCellClass:[MMGridCell class] forCellWithReuseIdentifier:@"GridCell"];
    [self.spreadSheetView registerCellClass:[MMTopRowCell class] forCellWithReuseIdentifier:@"TopRowCell"];
    [self.spreadSheetView registerCellClass:[MMLeftColumnCell class] forCellWithReuseIdentifier:@"LeftColumnCell"];
    
    // Set the delegate & datasource for the spreadsheet view.
    self.spreadSheetView.delegate = self;
    self.spreadSheetView.dataSource = self;
    
    // Add the spreadsheet view as a subview.
    [self.view addSubview:self.spreadSheetView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - MMSpreadsheetViewDataSource

- (CGSize)spreadsheetView:(MMSpreadsheetView *)spreadsheetView sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat leftColumnWidth = 150.0f;
    CGFloat topRowHeight = 150.0f;
    CGFloat gridCellWidth = 124.0f;
    CGFloat gridCellHeight = 103.0f;
    
    // Upper left.
    if (indexPath.mmSpreadsheetRow == 0 && indexPath.mmSpreadsheetColumn == 0) {
        return CGSizeMake(leftColumnWidth, topRowHeight);
    }
    
    // Upper right.
    if (indexPath.mmSpreadsheetRow == 0 && indexPath.mmSpreadsheetColumn > 0) {
        return CGSizeMake(gridCellWidth, topRowHeight);
    }
    
    // Lower left.
    if (indexPath.mmSpreadsheetRow > 0 && indexPath.mmSpreadsheetColumn == 0) {
        return CGSizeMake(leftColumnWidth, gridCellHeight);
    }
    
    return CGSizeMake(gridCellWidth, gridCellHeight);
}

- (NSInteger)numberOfRowsInSpreadsheetView:(MMSpreadsheetView *)spreadsheetView {
    NSInteger rows = [self.tableData count];
    return rows;
}

- (NSInteger)numberOfColumnsInSpreadsheetView:(MMSpreadsheetView *)spreadsheetView {
    NSArray *rowData = [self.tableData lastObject];
    NSInteger cols = [rowData count];
    return cols;
}

- (UICollectionViewCell *)spreadsheetView:(MMSpreadsheetView *)spreadsheetView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = nil;
    if (indexPath.mmSpreadsheetRow == 0 && indexPath.mmSpreadsheetColumn == 0) {
        // Upper left.
        cell = [spreadsheetView dequeueReusableCellWithReuseIdentifier:@"GridCell" forIndexPath:indexPath];
        MMGridCell *gc = (MMGridCell *)cell;
        UIImageView *logo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo"]];
        [gc.contentView addSubview:logo];
        logo.center = gc.contentView.center;
        gc.valueLabel.numberOfLines = 0;
        cell.backgroundColor = [UIColor colorWithWhite:0.9f alpha:1.0f];
    }
    else if (indexPath.mmSpreadsheetRow == 0 && indexPath.mmSpreadsheetColumn > 0) {
        // Upper right.
        cell = [spreadsheetView dequeueReusableCellWithReuseIdentifier:@"TopRowCell" forIndexPath:indexPath];
        MMTopRowCell *tr = (MMTopRowCell *)cell;
        tr.textLabel.text = [NSString stringWithFormat:@"TR: %li", (long)indexPath.mmSpreadsheetColumn];
        cell.backgroundColor = [UIColor whiteColor];
    }
    else if (indexPath.mmSpreadsheetRow > 0 && indexPath.mmSpreadsheetColumn == 0) {
        // Lower left.
        cell = [spreadsheetView dequeueReusableCellWithReuseIdentifier:@"LeftColumnCell" forIndexPath:indexPath];
        MMLeftColumnCell *lc = (MMLeftColumnCell *)cell;
        lc.textLabel.text = [NSString stringWithFormat:@"Column: %li", (long)indexPath.mmSpreadsheetRow];
        BOOL isDarker = indexPath.mmSpreadsheetRow % 2 == 0;
        if (isDarker) {
            cell.backgroundColor = [UIColor colorWithRed:222.0f / 255.0f green:243.0f / 255.0f blue:250.0f / 255.0f alpha:1.0f];
        } else {
            cell.backgroundColor = [UIColor colorWithRed:233.0f / 255.0f green:247.0f / 255.0f blue:252.0f / 255.0f alpha:1.0f];
        }
    }
    else {
        // Lower right.
        cell = [spreadsheetView dequeueReusableCellWithReuseIdentifier:@"GridCell" forIndexPath:indexPath];
        SSModelData *dataModel = [self.modelController dataModelForX:indexPath.row-1 andY:indexPath.section-1];
        
        MMGridCell *gc = (MMGridCell *)cell;
        if(dataModel.value || dataModel.type != SpreadsheetFormulaeValue){
            //[gc.valueLabel setText:[NSString stringWithFormat:@"(%d-%d, %d):%.0f", (int)indexPath.row, (int)indexPath.section, dataModel.index, dataModel.value]];
            [gc.valueLabel setText:[NSString stringWithFormat:@"%.0f", dataModel.value]];
        }
        else{
            [gc.valueLabel setText:@""];
        }
        if(SpreadsheetFormulaeValue==dataModel.type){
            [gc.attributeLabel setHidden:YES];
            [gc.formulaeLabel  setHidden:YES];
        }
        else{
            [gc.formulaeLabel  setHidden:NO];
            [gc.attributeLabel setHidden:NO];
            [gc.formulaeLabel setText:[self formulaeToString:dataModel.type]];
            [gc.attributeLabel setText:[dataModel.range rangeDescription]];
        }
        
        [self cell:gc setBackgroundForIndexPath:indexPath];
        
        //
        if(self.selectedDataModel){
            NSArray *objects = [self.modelController getObjectsForRange:self.selectedDataModel.range];
            if([objects containsObject:dataModel]){
                cell.backgroundColor = [UIColor yellowColor];
            }
        }
        
        
    }
    
    return cell;
}


#pragma mark - MMSpreadsheetViewDelegate

- (void)spreadsheetView:(MMSpreadsheetView *)spreadsheetView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.selectedGridCells containsObject:indexPath]) {
        [self.selectedGridCells removeObject:indexPath];
        [spreadsheetView deselectItemAtIndexPath:indexPath animated:YES];
        [self removeSelectedDataModel];
    } else {
        [self.selectedGridCells removeAllObjects];
        [self.selectedGridCells addObject:indexPath];
        
        SSModelData *dataModel = [self.modelController dataModelForX:indexPath.row-1 andY:indexPath.section-1];
        if(dataModel.type != SpreadsheetFormulaeValue){
            [self dataMode:dataModel selected:YES];
        }
        
    }
}

- (void)dataMode:(SSModelData*)dataModel selected:(BOOL)selected
{
    [self removeSelectedDataModel];
    if(selected){
        self.selectedDataModel = dataModel;
        NSArray *objects = [self.modelController getObjectsForRange:dataModel.range];
        NSArray *visibleIndexPaths = [self.spreadSheetView.selectedItemCollectionView indexPathsForVisibleItems];
        [objects enumerateObjectsUsingBlock:^(SSModelData *obj, NSUInteger idx, BOOL *stop) {
            NSIndexPath *path = [NSIndexPath indexPathForRow:obj.x inSection:obj.y];
            if([visibleIndexPaths containsObject:path]){
                MMGridCell *cell = (MMGridCell*)[self.spreadSheetView.selectedItemCollectionView cellForItemAtIndexPath:path];
                [cell setBackgroundColor:[UIColor yellowColor]];
            }
        }];
    }

}

- (BOOL)spreadsheetView:(MMSpreadsheetView *)spreadsheetView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (BOOL)spreadsheetView:(MMSpreadsheetView *)spreadsheetView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
    return NO;
}

- (void)spreadsheetView:(MMSpreadsheetView *)spreadsheetView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
    NSMutableArray *rowData = [self.tableData objectAtIndex:indexPath.mmSpreadsheetRow];
    if (action == @selector(cut:)) {
        self.cellDataBuffer = [rowData objectAtIndex:indexPath.row];
        [rowData replaceObjectAtIndex:indexPath.row withObject:@""];
        [spreadsheetView reloadData];
    } else if (action == @selector(copy:)) {
        self.cellDataBuffer = [rowData objectAtIndex:indexPath.row];
    } else if (action == @selector(paste:)) {
        if (self.cellDataBuffer) {
            [rowData replaceObjectAtIndex:indexPath.row withObject:self.cellDataBuffer];
            [spreadsheetView reloadData];
        }
    }
}

#pragma mark - Private method

- (NSString*)formulaeToString:(SpreadsheetFormulaeType)type
{
    NSString *typeString = nil;
    switch (type) {
        case SpreadsheetFormulaeValue: typeString = @"value"; break;
        case SpreadsheetFormulaeSum:   typeString = @"sum"; break;
        case SpreadsheetFormulaeCount: typeString = @"count"; break;
        case SpreadsheetFormulaeMax:   typeString = @"max"; break;
        case SpreadsheetFormulaeMin:   typeString = @"min"; break;
        default:                       typeString = @"unknown"; break;
    }
    return typeString;
}

- (void)cell:(MMGridCell*)cell setBackgroundForIndexPath:(NSIndexPath*)indexPath
{
    BOOL isDarker = indexPath.mmSpreadsheetRow % 2 == 0;
    if (isDarker) {
        cell.backgroundColor = [UIColor colorWithRed:242.0f / 255.0f green:242.0f / 255.0f blue:242.0f / 255.0f alpha:1.0f];
    } else {
        cell.backgroundColor = [UIColor colorWithRed:250.0f / 255.0f green:250.0f / 255.0f blue:250.0f / 255.0f alpha:1.0f];
    }
}

- (void)removeSelectedDataModel
{
    if(self.selectedDataModel){
        NSArray *objects = [self.modelController getObjectsForRange:self.selectedDataModel.range];
        NSArray *visibleIndexPaths = [self.spreadSheetView.selectedItemCollectionView indexPathsForVisibleItems];
        [objects enumerateObjectsUsingBlock:^(SSModelData *obj, NSUInteger idx, BOOL *stop) {
            NSIndexPath *path = [NSIndexPath indexPathForRow:obj.x inSection:obj.y];
            if([visibleIndexPaths containsObject:path]){
                MMGridCell *cell = (MMGridCell*)[self.spreadSheetView.selectedItemCollectionView cellForItemAtIndexPath:path];
                [self cell:cell setBackgroundForIndexPath:path];
            }
        }];
    }
    self.selectedDataModel = nil;
}

@end
