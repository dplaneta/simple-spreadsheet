//
//  SpreadsheetViewController.h
//  Simple Spreadsheet
//
//  Created by Dawid on 03/08/2014.
//  Copyright (c) 2014 Dawid. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SpreadsheetModelController;
@interface SpreadsheetViewController : UIViewController
- (instancetype)initWithModelController:(SpreadsheetModelController*)modelController;
@end
