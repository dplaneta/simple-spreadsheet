//
//  SpreedsheetRange.m
//  Simple Spreadsheet
//
//  Created by Dawid on 05/08/2014.
//  Copyright (c) 2014 Dawid. All rights reserved.
//

#import "SSRange.h"

@implementation SSRange

- (instancetype)init
{
    self=[super init];
    if(self){
        _x1=0;
        _y1=0;
        _x2=0;
        _y2=0;
    }
    return self;
}

- (NSString*)debugDescription
{
    return [NSString stringWithFormat:@"(%lu, %lu) (%lu, %lu)", (unsigned long)_x1, (unsigned long)_y1, (unsigned long)_x2, (unsigned long)_y2];
}

- (NSString*)rangeDescription
{
    return [NSString stringWithFormat:@"(%lu, %lu) (%lu, %lu)", (unsigned long)_x1+1, (unsigned long)_y1+1, (unsigned long)_x2+1, (unsigned long)_y2+1];
}

@end
