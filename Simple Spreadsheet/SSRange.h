//
//  SpreedsheetRange.h
//  Simple Spreadsheet
//
//  Created by Dawid on 05/08/2014.
//  Copyright (c) 2014 Dawid. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SSRange : NSObject
@property (nonatomic, assign) NSUInteger x1, y1;
@property (nonatomic, assign) NSUInteger x2, y2;
- (NSString*)rangeDescription;
@end
