//
//  SpreadsheetModelData+Rand.m
//  Simple Spreadsheet
//
//  Created by Dawid on 03/08/2014.
//  Copyright (c) 2014 Dawid. All rights reserved.
//

#include <stdlib.h>
#import "SSModelData+Rand.h"

@implementation SSModelData (Rand)
- (void)setRandomValue
{
    NSRange range = NSMakeRange(0, 10);
    self.value = (NSInteger)(range.location + rand() % (range.length-range.location));
}

- (void)setRandomFormulae
{
    self.type = (SpreadsheetFormulaeType)(1+rand()%4);
}

- (void)setRandomRangeForGridWidth:(NSUInteger)width andNumberOfObjects:(NSUInteger)limit
{
    self.range.x1 = self.x;
    self.range.y1 = self.y;
    self.range.x2 = 1+ self.x + rand() % (int)(width-self.x);
    self.range.y2 = 1+ self.y + rand() % (int)(roundf(limit/width)-self.y);
    
    self.range.x2 = MIN(self.range.x2, width-1);
    self.range.y2 = MIN(self.range.y2, roundf(limit/width)-1);
}
@end
