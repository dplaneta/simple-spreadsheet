//
//  SpreadsheetModelData+Rand.h
//  Simple Spreadsheet
//
//  Created by Dawid on 03/08/2014.
//  Copyright (c) 2014 Dawid. All rights reserved.
//

#import "SSModelData.h"
#import "SSRange.h"

@interface SSModelData (Rand)
- (void)setRandomValue;
- (void)setRandomFormulae;
- (void)setRandomRangeForGridWidth:(NSUInteger)width andNumberOfObjects:(NSUInteger)limit;
@end
