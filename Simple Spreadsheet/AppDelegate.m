//
//  AppDelegate.m
//  Simple Spreadsheet
//
//  Created by Dawid on 03/08/2014.
//  Copyright (c) 2014 Dawid. All rights reserved.
//

#import "AppDelegate.h"
#import "SpreadsheetModelController.h"
#import "SpreadsheetViewController.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    SpreadsheetModelController *modelController = [[SpreadsheetModelController alloc] initWithRows:10 andCols:10];
    [modelController generateRandomGrid];
    
    SpreadsheetViewController *viewController   = [[SpreadsheetViewController alloc] initWithModelController:modelController];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self.window setRootViewController:viewController];
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;
}

@end
