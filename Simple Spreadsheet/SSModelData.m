//
//  SpreadsheetModelData.m
//  Simple Spreadsheet
//
//  Created by Dawid on 03/08/2014.
//  Copyright (c) 2014 Dawid. All rights reserved.
//

#import "SSModelData.h"

@interface SSModelData ()
@property (nonatomic, assign) NSUInteger x, y, index;
@end

@implementation SSModelData

- (instancetype)initWithIndex:(NSUInteger)index andGridWidth:(NSUInteger)width
{
    self=[super init];
    if(self){
        _index = index;
        _x     = index % width;
        _y     = (NSUInteger)roundf(index / width);
        _range = [SSRange new];
    }
    return self;
}

@end
