//
//  SpreadsheetModelController.m
//  Simple Spreadsheet
//
//  Created by Dawid on 03/08/2014.
//  Copyright (c) 2014 Dawid. All rights reserved.
//

#import "SpreadsheetModelController.h"
#import "SSModelData.h"
#import "SSModelData+Rand.h"

@interface SpreadsheetModelController ()
@property (nonatomic, assign) NSUInteger rows;
@property (nonatomic, assign) NSUInteger cols;
@property (nonatomic, strong) NSMutableArray *grid;
@end

@implementation SpreadsheetModelController

- (instancetype)initWithRows:(NSUInteger)rows andCols:(NSUInteger)cols;
{
    self=[super init];
    if(self){
        _rows = rows;
        _cols = cols;
    }
    return self;
}


#pragma mark - Public methods

- (void)generateRandomGrid
{
    time_t t;
    srand((unsigned) time(&t));
    
    NSUInteger capacity = _rows*_cols;
    _grid = [[NSMutableArray alloc] initWithCapacity:capacity];
    for (NSUInteger i=0; i<capacity; ++i) {
        SSModelData *dataModel = [[SSModelData alloc] initWithIndex:i andGridWidth:_rows];
        if(0==rand()%9){
            [dataModel setRandomRangeForGridWidth:_rows andNumberOfObjects:capacity];
            [dataModel setRandomFormulae];
        }
        else{
            [dataModel setRandomValue];
        }
        [_grid addObject:dataModel];
    }
    [self calculateFormulaeValues];
}

- (SSModelData*)dataModelForX:(NSUInteger)x andY:(NSUInteger)y
{
    NSUInteger index = [self getIndexForX:x andY:y];
    SSModelData *dataModel = _grid[index];
    return dataModel;
}

- (NSArray*)getObjectsForRange:(SSRange*)range
{
    NSMutableArray *array = [NSMutableArray new];
    for(NSUInteger y=range.y1; y<1+range.y2; ++y){
        for(NSUInteger x=range.x1; x<1+range.x2; ++x){
            NSUInteger index = [self getIndexForX:x andY:y];
            [array addObject:_grid[index]];
        }
    }
    return array;
}


#pragma mark - Private API

- (NSUInteger)getIndexForX:(NSUInteger)x andY:(NSUInteger)y
{
    return  x + _rows*y;
}

- (void)calculateFormulaeValues
{
    for(int i=_rows*_cols-1; i>=0; --i){
        SSModelData *obj = _grid[i];
        if(obj.type != SpreadsheetFormulaeValue){
            obj.value = [self calculateValueForModelData:obj];
        }
    }
}

- (CGFloat)calculateValueForModelData:(SSModelData*)modelData
{
    __block CGFloat value=0.0f;
    NSArray *cells = [self getObjectsForRange:modelData.range];
    [cells enumerateObjectsUsingBlock:^(SSModelData *obj, NSUInteger idx, BOOL *stop) {
        if(modelData != obj) value = [self formulaeType:modelData.type value1:value value2:obj.value];
    }];
    return value;
}

- (CGFloat)formulaeType:(SpreadsheetFormulaeType)type value1:(CGFloat)value1 value2:(CGFloat)value2
{
    switch (type) {
        case SpreadsheetFormulaeSum:   value1+=value2;              break;
        case SpreadsheetFormulaeCount: value1+=(value2!=0.0f);      break;
        case SpreadsheetFormulaeMax:   {if(value2) value1=MAX(value1, value2);} break;
        case SpreadsheetFormulaeMin:   {
                                            if(value2){
                                                if(value1==0.0f) value1 = value2;
                                                else value1=MIN(value1, value2);
                                            }
                                        } break;
        default: break;
    }
    return value1;
}

@end
