//
//  SpreadsheetModelData.h
//  Simple Spreadsheet
//
//  Created by Dawid on 03/08/2014.
//  Copyright (c) 2014 Dawid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SSRange.h"

typedef NS_ENUM(NSInteger, SpreadsheetFormulaeType) {
    SpreadsheetFormulaeValue=0,
    SpreadsheetFormulaeSum,
    SpreadsheetFormulaeCount,
    SpreadsheetFormulaeMax,
    SpreadsheetFormulaeMin
};

@interface SSModelData : NSObject
@property (nonatomic, assign, readonly) NSUInteger x, y, index;
@property (nonatomic, assign) CGFloat value;
@property (nonatomic, assign) SpreadsheetFormulaeType type;
@property (nonatomic, strong) SSRange *range;

- (instancetype)initWithIndex:(NSUInteger)index andGridWidth:(NSUInteger)width;
@end
