//
//  SpreadsheetModelController.h
//  Simple Spreadsheet
//
//  Created by Dawid on 03/08/2014.
//  Copyright (c) 2014 Dawid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SSModelData.h"

@interface SpreadsheetModelController : NSObject
@property (nonatomic, assign, readonly) NSUInteger rows;
@property (nonatomic, assign, readonly) NSUInteger cols;

- (instancetype)initWithRows:(NSUInteger)rows andCols:(NSUInteger)cols;
- (void)generateRandomGrid;

- (SSModelData*)dataModelForX:(NSUInteger)x andY:(NSUInteger)y;

- (NSArray*)getObjectsForRange:(SSRange*)range;

@end
